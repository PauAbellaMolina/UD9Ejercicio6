package metodos;

import dto.Cine;
import dto.Espectador;

public class Metodos {
	//Metodo que genera una lista de espectadores con informacion aleatoria de una lista de valores
	public static Espectador[] generarListaEspectadores(Espectador listaEspectadores[]) {
		//Listas de nombres, edades y precios
		String nombres[] = new String[] {"Pau", "Sergi", "Dani", "Jose", "Abel", "Alberto", "Oscar", "Aitor", "Gerard", "Pol"};
		int edad[] = new int[] {10, 14, 18, 22, 26, 30, 34, 38, 42, 46};
		double dinero[] = new double[] {3.5, 5, 8.5, 10, 12.5, 15, 18.9, 20, 25, 50};
		
		//Bucle que llena el array listaEspectadores con espectadores con informacion aleatoria
		for (int cont = 0; cont < listaEspectadores.length; cont++) {
			listaEspectadores[cont] = new Espectador(nombres[generarNumeroRandom()], edad[generarNumeroRandom()], dinero[generarNumeroRandom()]);
		}
		
		return listaEspectadores;
	}
	
	//Metodo que devuelve un numero aleatorio entre 0 y 9
	public static int generarNumeroRandom() {
		return (int) (Math.random() * 10);
	}
	
	//Metodo que sienta los espectadores que pueden entrar en el cine en posciones aleatorias de la sala
	public static Espectador[][] sentarEspectadores(Espectador listaEspectadores[], Espectador sala[][], Cine cine) {
		for (int cont = 0; cont < listaEspectadores.length; cont++) {
			int filaAleatoria = generarNumeroRandom();
			int columnaAleatoria = generarNumeroRandom();
			
			//Si la posicion aleatoria esta vacia, sigue
			if (sala[filaAleatoria][columnaAleatoria] == null) {
				//Si el espectador cumple los requisitos de entrada, sientalo en la posicion aleatoria
				if (comprovacionEspectador(listaEspectadores[cont], cine)) {
					sala[filaAleatoria][columnaAleatoria] = listaEspectadores[cont];
				}
			}
		}
		
		return sala;
	}
	
	//Metodo que devuelve si el espectador recibido puede entrar en el cine recibido
	public static boolean comprovacionEspectador(Espectador espectador, Cine cine) {
		if (espectador.getEdad() >= cine.getPeliculaReproduciendo().getEdadMinima() && espectador.getDinero() >= cine.getPrecio()) {
			return true;
		}
		
		return false;
	}
	
	//Metodo que muestra la sala del cine
	public static void mostrarSala(Espectador sala[][]) {
		System.out.println("Sala: ");
		for (int i = 0; i < sala.length; i++) {
            for (int j = 0; j < sala[0].length; j++) {
            	if (sala[i][j] == null) {
            		System.out.print("VACIO" + "\t");
            	} else {
            		System.out.print(sala[i][j] + "\t");
            	}
            }
            System.out.println("");
        }
	}
}
