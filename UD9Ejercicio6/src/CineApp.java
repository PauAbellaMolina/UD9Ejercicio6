import dto.Cine;
import dto.Espectador;
import dto.Pelicula;
import metodos.Metodos;

public class CineApp {

	public static void main(String[] args) {
		//Creamos los objetos a usar
		Espectador sala[][] = new Espectador[10][10];
		Espectador listaEspectadores[] = new Espectador[40];
		Pelicula pelicula = new Pelicula("El Rey Leon", 120, 16, "Quentin Tarantino");
		Cine cine = new Cine(pelicula, 10);
		
		//Generamos una lista de espectadores de manera aleatoria
		listaEspectadores = Metodos.generarListaEspectadores(listaEspectadores);
		
		//Sentamos los espectadores de la lista en los asientos de la sala
		sala = Metodos.sentarEspectadores(listaEspectadores, sala, cine);
		
		//Mostramos la infomacion del cine y el estado de los asientos de la sala
		System.out.println("Pelicula: " + cine.getPeliculaReproduciendo());
		System.out.println("Precio: " + cine.getPrecio());
		Metodos.mostrarSala(sala);
	}
}
