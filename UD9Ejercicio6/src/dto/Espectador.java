package dto;

public class Espectador {

	//Atributos
	protected String nombre;
	protected int edad;
	protected double dinero;
	
	//Constructor por defecto
	public Espectador() {
		this.nombre="";
		this.edad=0;
		this.dinero=0;
	}

	//Constructor con todos los atributos excepto entregado
	public Espectador(String nombre, int edad, double dinero) {
		this.nombre=nombre;
		this.edad=edad;
		this.dinero=dinero;
	}

	//Metodo toString
	public String toString() {
		return nombre;
	}

	//Getters
	public String getNombre() {
		return nombre;
	}


	public int getEdad() {
		return edad;
	}

	public double getDinero() {
		return dinero;
	}
}
