package dto;

public class Cine {

	//Atributos
	protected Pelicula peliculaReproduciendo;
	protected double precio;
	
	//Constructor por defecto
	public Cine() {
		this.precio = 0;
	}
	
	//Constructor con todos los atributos excepto entregado
	public Cine(Pelicula pelicula, double precio) {
		this.peliculaReproduciendo=pelicula;
		this.precio=precio;
	}

	//Getters
	public Pelicula getPeliculaReproduciendo() {
		return peliculaReproduciendo;
	}

	public double getPrecio() {
		return precio;
	}
}
