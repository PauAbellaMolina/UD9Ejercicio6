package dto;

public class Pelicula {

	//Atributos
	protected String titulo;
	protected int duracion;
	protected int edadMinima;
	protected String director;
	
	//Constructor por defecto
	public Pelicula() {
		this.titulo="";
		this.duracion=0;
		this.edadMinima=0;
		this.director="";
	}

	//Constructor con todos los atributos excepto entregado
	public Pelicula(String titulo, int duracion, int edadMinima, String director) {
		this.titulo=titulo;
		this.duracion=duracion;
		this.edadMinima=edadMinima;
		this.director=director;
	}

	//Metodo toString
	public String toString() {
		return titulo;
	}

	//Getter edad minima
	public int getEdadMinima() {
		return edadMinima;
	}
}
